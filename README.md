# Mapa conceptual "Conjuntos, Aplicaciones y funciones (2002)"


```plantuml 
@startmindmap 
*[#C1FFA0] Conjuntos

 *[#lightblue] Idea de conjunto y elemento

  *_ construyen
   * Teoria de conjuntos
    * Caracteristicas
     * Racional
     * Abstracta
    * Inclusion de conjuntos
     * Relacion de orden
      *_ entre 
       * Numeros 
     *_ todos un
      * Conjunto A
       *_ dentro de un
        * Conjunto B  
     * Regla de pertenencia  

    * Operaciones
     *_ crean
      * Conjuntos complejos
       *_ a partir de
        * Conjuntos elementales

     * Interseccion
      *_  elementos con
       * Pertenecia simultanea
        *_ a 
         * Diferentes conjuntos
     * Complementacion
      *_ elementos sin
       * complementar
        *_ a un dado
         * Conjunto 
     * Union
      *_ elementos de 
       * pertenecia
        *_ a al menos un
         * Conjunto
     * Compuestas
      * Diferencia de conjuntos     
    
    * Tipos especiales
     * Universal
      *_ de referencia para
       * Justificar la teoria
        *_ incluye
         * Todos los elementos

     * Vacio
      *_ necesidad logica
       * Conjunto sin elementos

    * Representacion
     *_ por medio de 
      * Diagramas de Venn 
       *_ util para
        * Ilustracion
       *_ no util para
        * Demostracion   
      
    * Cardinalidad 
     *_ sirve de union
      * Teoria de conjuntos
       *_ y 
        * Teoria de numeros naturales
     *_ es el 
      * Numero de elementos   

   
 *[#lightblue] Aplicaciones 
  *_ es una
   * Transformacion
    *_ de cada
     * Elemento
      *_ de en un 
       * Conjunto inicial 
        *_ en un 
         * Unico elemento
          *_ de un
           * Conjunto final

    * Transformacion de Transformacion
     *_ hacer primero una
      * Aplicacion
       *_ y otra la
        * Transforma
     *_ lleva a 
      * Composicion de aplicaciones   
    * Imagen e inversa 
   
  * Funciones
   * Conjuntos de numeros
    *_ que sufren 
     * Transformacion
   *_ genera 
    * Serie de puntos
     *_ produce
      * Figura grafica 

  
  @endmindmap 
  ```

# Mapa conceptual "Funciones (2010)"

```plantuml 
@startmindmap 
*[#C1FFA0] Funciones
 *_ es una
  *[#lightblue] Aplicacion especial
   *_ se produce en
    * Conjunto de numeros
     *_ particularmente en
      * Conjunto de numeros reales

   *_ cuenta con
    * Representacion grafica
     *_ por medio de
      * Plano Cartesiano
       * Eje X
        * Numeros reales
       * Eje Y
        * Imagen  
  
   * Crecientes
    *_ al aumentar
     * Valor de X
      *_ aumenta
       * Valor de la imagen

   * Decrecientes
    *_ al aumentar
     * Valor de X
      *_ disminuye
       * Valor de la imagen
   
   * Limite
    *_ conforme los
     * Valores de X
      *_ se acercan a un
       * Punto 
        *_ valores de las
         * Imagenes
          *_ se acercan a un
           * Determinado valor
    * Limites laterales
     * Por la izquierda
      *_ se expresa con un
       * Signo menos
     * Por la derecha
      *_ se expresa con un
       * Signo mas
     * Iguales
      *_ si existen ambos
       * Limites
        *_ en un
         * Punto
    * Aplicaciones
     *_ para estudiar la
      * Continuidad
       *_ de una 
        * Funcion
     * para calcular las
      * Asintotas
       * Asintota horizontal
       * Asintota vertical
       * Asintota oblicua      

   * Continuidad
  
    *_ mas
     * Uniforme
      *_ mejor 
       * Comportamiento
      *_ no contiene
       * Saltos
      *_ sin
       * Oscilaciones indefinidas  
    *_ hace que la
     * Funcion
      *_ sea mas
       * Manejable

   * Discontinuidad  
    *_ no
     * Uniforme
      *_ ya que presenta
       * Saltos
    *_ condiciona el
     * Limite de una funcion
      * Tipos
       * Discontinuidad evitable
       * Discontinuidad no evitable o esencial  


 *[#lightblue] Calculo diferencial
  *_ para obtener
   * Funciones sencillas
    *_ a partir de
     * Funciones complejas
  * Derivada
   *_ resuelve el problema de
    * Aproximacion
     *_ de una
      * Funcion compleja
       *_ mediante una 
        * Funcion simple
     * Ejemplo
      * Movimiento
       *_ si una funcion
        * Representa la posicion
         *_ respecto al 
          * Tiempo
         *_ la
          * Derivada
           *_ es la
            * Velocidad  

   * Recta tangente
    *_ en un 
     * Punto dado
      *_ para una 
       * Aproximacion
    *_ puede interpretarse
     * Geometricamente
   *_ surge de la necesidad de
    * Aproximacion al cambio
     *_ es la
      * Razon del cambio
       *_ con el que
        * Varia el valor
         *_ de una
          * Funcion
    *_ es un concepto
     * Local
      *_ se calcula como
       * Limite de rapidez
        *_ de 
         * Cambio medio      
   *_ busca
    * Restar complejidad         

   
  
 
  @endmindmap 
  ```


# Mapa conceptual "La matemática del computador (2002)"


```plantuml 
@startmindmap 
*[#C1FFA0] La matemática del computador 

 *[#lightblue] Aritmetica Finita
  *_ el computar presenta
   * Problemas
    *_ al procesar
     * Valores no finitos
    *_ en la 
     * Idea del numero aproximado
    * Idea de error 
     *_ al aproximar
      * Numero real
       *_ con cantidad
        * Finita de cifras
    *_ en la 
     * Idea de digitos significativos
      *_ para medir
       * Precision relativa
        *_ de un
         * Valor
      *_ sirven para aportar
       * Informacion concreta
    *_ al 
     * Truncar
      *_ es cortar
       * Numero de decimales
      *_ descarta digitos
       * Menos significativos 
      * Redondear 
       *_ intentar reducir
        * Error medio

 
  * Digitos binarios
   *_ sirve para
    * Representaciones
     * Interna de numeros
     * Magnitud signo
      *_ representa el
       * Signo del entero
        *_ separado de su
         * Magnitud
      *_ separa un
       * Bit
        *_ para representar el
         * Signo
        *_ asigna el valor de
         * Cero
          *_ para representar
           * Positivo 
        *_ asigna el valor de
         * Uno
          *_ para representar
           * Negativo    
     * En exceso
      *_ para representar
       * Exponentes
        *_ en caso de
         * Coma flotante

     * Complemento dos
      *_ nos permite 
       * codificar en binario
        *_ en punto fijo con
         * 8 bits
      * Ventajas
       *_ no posee
        * Doble representación
         *_ de
          * Ceros
       *_ Permite operar
        * Aritméticamente

      * Desventajas
       *_  posee
        * Rango asimetrico
         

   *_ ayuda a la
    * Materializacion
     *_ de un 
      * Abstracto
       * Numeros
        *_ para
         * Operaciones matematicas
       * Letras
        *_ para
         * Textos
       * Signos
        *_ para
         * Dualidad de valores
       * Instrucciones
        *_ para
         * Programacion

   * Sistema en base dos
    *_ en el se basan
     * Computadores
      * Instrucciones
       * Encendido
        *_ para el paso de
         * Voltaje
       * Apagado
        *_ para el bloqueo de
         * Voltaje  
    *_ de facil  
     * Representacion
      *_ en el
       * Mundo fisico
    *_ mediante
     * Valores
      *_ de
       * Ceros
        *_ y
         * Unos
    
    *_ otros
     * Sistemas de numeracion
      * Octal
      * Hexadecimal
      *_ para simplificar
       * Representacion de numeros
        *_ de 
         * Gran magnitud
      *_ utilizar menos
       * Simbolos
       

        
  
  @endmindmap 
  ```
